var multiplayerState = {
    create: function () {
        console.log('Multiplayer');

        game.add.button( (game.width*20)/100, (game.height*40)/100, 'blueButton', this.createServerSelect);
        game.add.text((game.width*23)/100, (game.height*42)/100, 'Create Server', {
            font: '20px Arial', fill: '#000000'
        });

        game.add.text((game.width*20)/100, (game.height*50)/100, 'OR', {
            font: '20px Arial', fill: '#ffffff'
        });


        game.add.text((game.width*20)/100, (game.height*60)/100, 'Type Server ID to Join', {
            font: '20px Arial', fill: '#ffffff'
        });
        game.add.text((game.width*20)/100, (game.height*66)/100, '__________________', {
            font: '20px Arial', fill: '#ffffff'
        });

        game.add.button( (game.width*20)/100, (game.height*75)/100, 'yellowButton');
        game.add.text((game.width*24)/100, (game.height*77)/100, 'Join Server', {
            font: '20px Arial', fill: '#000000'
        });


        game.add.text((game.width*50)/100, (game.height*50)/100, 'OR', {
            font: '20px Arial', fill: '#ffffff'
        });

        game.add.button( (game.width*60)/100, (game.height*50)/100, 'yellowButton');
        game.add.text((game.width*62)/100, (game.height*51)/100, 'Play With Random\nPlayer', {
            font: '15px Arial', fill: '#000000'
        });

        serveridArray = [];
        serverid = '';

        displayServerid = game.add.text((game.width*42)/100, (game.height*65)/100, playerName, {
            font: '25px Arial', fill: '#ffffff'
        });

        game.add.button( (game.width*85)/100, (game.height*85)/100, 'homeButton', this.backToHome);

    },
    backToHome: function () {
        game.state.start('menu');
    },
    createServerSelect: function () {
        game.state.start('createServer');
    }
};