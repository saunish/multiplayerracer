var playerWinState = {
    create: function () {
        if(whichGameMode == 2){
            game.add.text((game.width*10)/100, (game.height*10)/100, playerName + ' won against ' + opponentName, {
                font: '30px Arial', fill: '#ffffff'
            });
        }
        else {
            game.add.text((game.width*10)/100, (game.height*10)/100, playerName + ' won against Computer', {
                font: '30px Arial', fill: '#ffffff'
            });
        }

        game.add.button( (game.width*85)/100, (game.height*85)/100, 'homeButton', this.backToHome);
        //game.add.button( (game.width*85)/100, (game.height*70)/100, 'leaderboardIcon', this.leaderboardSelect);
    },
    backToHome: function () {
        game.state.start('menu');
    },
    leaderboardSelect: function () {
        game.state.start('leaderboard');
    }
};