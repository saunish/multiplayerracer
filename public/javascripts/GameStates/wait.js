var waitState = {
    create: function () {
        time_count = 0;
        waitText = game.add.text((game.width*10)/100, (game.height*10)/100, playerName + '  vs  ' + opponentName, {
            font: '30px Arial', fill: '#ffffff'
        });

        countDownText = game.add.text((game.width*10)/100, (game.height*20)/100, '', {
            font: '30px Arial', fill: '#ffffff'
        });
    },
    update: function () {
        if(time_count/60 == 0) countDownText.text = 'game starts in 3 seconds';
        else if(time_count/60 == 1) countDownText.text = 'game starts in 2 seconds';
        else if(time_count/60 == 2) countDownText.text = 'game starts in 1 seconds';
        else if(time_count/60 == 3) game.state.start('gamePlay');
        time_count++;
    }
};