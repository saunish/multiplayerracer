var loadState = {
    preload : function () {

        loadingLable = game.add.text(80, 150, 'loading...',{
            font : '30px Courier', fill: '#ffffff'});

        //tilemap Json
        game.load.tilemap('desertGround', 'assets/desertGround.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.tilemap('grassGround', 'assets/grassGround.json', null, Phaser.Tilemap.TILED_JSON);

        //tilemap Image
        game.load.image('desertBackground', 'assets/Backgrounds/colored_desert.png');
        game.load.image('grassBackground', 'assets/Backgrounds/colored_grass.png');
        game.load.image('sandTile', 'assets/Ground/Sand/sandMid.png');
        game.load.image('grassTile', 'assets/Ground/Grass/grassMid.png');

        //object Image
        game.load.image('star', 'assets/Items/star.png');


        //Character Sprite
        game.load.atlas('dude', 'assets/Character/Player/player_tilesheet.png', 'assets/Character/Player/player_tilesheet.json');
        game.load.atlas('zombie', 'assets/Character/Zombie/zombie_tilesheet.png', 'assets/Character/Zombie/zombie_tilesheet.json');
        game.load.atlas('soldier', 'assets/Character/Soldier/soldier_tilesheet.png', 'assets/Character/Soldier/soldier_tilesheet.json');
        game.load.atlas('adventurer', 'assets/Character/Adventurer/adventurer_tilesheet.png', 'assets/Character/Adventurer/adventurer_tilesheet.json');


        //game icons
        game.load.image('gameStartIcon', 'assets/GameIcons/buttonStart.png');
        game.load.image('homeButton', 'assets/GameIcons/home.png');
        game.load.image('multiplayerIcon', 'assets/GameIcons/massiveMultiplayer.png');
        game.load.image('fullScreen', 'assets/GameIcons/larger.png');
        game.load.image('leaderboardIcon', 'assets/GameIcons/leaderboardsComplex.png');

        //game menu ui
        game.load.image('radioButton',          'assets/GameUI/grey_circle.png');
        game.load.image('radioButtonSelector',  'assets/GameUI/blue_tick.png');
        game.load.image('blueButton',           'assets/GameUI/blue_button03.png');
        game.load.image('yellowButton',         'assets/GameUI/yellow_button03.png');
        game.load.image('sliderMap',            'assets/GameUI/grey_sliderHorizontal.png');
        game.load.image('playerPointer',        'assets/GameUI/red_sliderUp.png');
        game.load.image('enemyPointer',         'assets/GameUI/yellow_sliderUp.png');
    },
    create : function () {
        console.log('load');
        game.state.start('menu');
    }
};