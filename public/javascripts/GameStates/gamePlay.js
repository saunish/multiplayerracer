var gamePlayState = {
    create: function () {
        console.log('gamePlay');

        game.physics.startSystem(Phaser.Physics.ARCADE);

        if(whichBackground == 1){
            map = game.add.tilemap('desertGround');

            map.addTilesetImage('sandMid', 'sandTile');
            map.addTilesetImage('colored_desert', 'desertBackground');

            layer = map.createLayer('DesertGround');
        }
        else if(whichBackground == 2){
            map = game.add.tilemap('grassGround');

            map.addTilesetImage('grassMid', 'grassTile');
            map.addTilesetImage('colored_grass', 'grassBackground');

            layer = map.createLayer('GrassGround');
        }
        map.setCollisionBetween(0, 3);

        if(whichGameMode == 1){
            enemy = game.add.sprite(map.objects.EnemyStart[0].x, map.objects.EnemyStart[0].y, 'zombie');
            game.physics.enable(enemy, Phaser.Physics.ARCADE);
            enemy.frameName = 'sprite6';
            enemy.body.gravity.y = 600;
            enemy.animations.add('walk', ['sprite15', 'sprite12']);
        }
        else if(whichGameMode == 2){
            enemy = game.add.sprite(map.objects.PlayerStart[0].x, map.objects.PlayerStart[0].y, opponentCharacter);
            game.physics.enable(enemy, Phaser.Physics.ARCADE);
            enemy.frameName = 'sprite6';
            enemy.body.gravity.y = 600;
            enemy.animations.add('walk', ['sprite15', 'sprite12']);
        }


        player = game.add.sprite(map.objects.PlayerStart[0].x, map.objects.PlayerStart[0].y, playerCharacter);
        game.physics.enable(player, Phaser.Physics.ARCADE);
        player.frameName = 'sprite7';
        player.body.gravity.y = 600;
        player.animations.add('walk', ['sprite15', 'sprite13']);

        star = game.add.sprite(map.objects.PlayerEnd[0].x, map.objects.PlayerEnd[0].y,'star');
        game.physics.enable(star, Phaser.Physics.ARCADE);


        var sliderMap = game.add.sprite((game.width*10)/100, (game.height*10)/100,'sliderMap');
        sliderMap.scale.setTo(4, 1.5);
        sliderMap.fixedToCamera = true;

        playerPointer = game.add.sprite(80, (game.height*10)/100,'playerPointer');
        game.physics.enable(playerPointer, Phaser.Physics.ARCADE);
        playerPointer.fixedToCamera = true;

        enemyPointer = game.add.sprite(840, (game.height*10)/100,'enemyPointer');
        game.physics.enable(enemyPointer, Phaser.Physics.ARCADE);
        enemyPointer.fixedToCamera = true;


        game.camera.follow(player);


        layer.resizeWorld();
        rightTapBool = true;
        leftTapBool = true;

        enemyStartBool = false;
    },
    update: function() {

        playerOrginalPosInPercent = (player.body.x*100)/layer.layer.widthInPixels;
        playerPointerPosRaw = (playerOrginalPosInPercent*760)/100;
        playerPointerPosNet = playerPointerPosRaw + 80;
        playerPointer.cameraOffset.x = playerPointerPosNet;

        enemyOrginalPosInPercent = (enemy.body.x*100)/layer.layer.widthInPixels;
        enemyPointerPosRaw = (enemyOrginalPosInPercent*760)/100;
        enemyPointerPosNet = enemyPointerPosRaw + 80;
        enemyPointer.cameraOffset.x = enemyPointerPosNet;

        game.physics.arcade.collide(player, layer);
        game.physics.arcade.collide(enemy, layer);

        leftkey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        rightkey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

        game.physics.arcade.overlap(player, star, this.playerWon, null, this);
        game.physics.arcade.overlap(enemy, star, this.enemyWon, null, this);

        if(whichGameMode == 1){
            if(enemyStartBool){
                enemy.animations.play('walk', 10, true);
                if(whichLevel == 1) enemy.body.velocity.x = 500;
                if(whichLevel == 2) enemy.body.velocity.x = 650;
                if(whichLevel == 3) enemy.body.velocity.x = 800;
            }
        }
        else if(whichGameMode == 2){
            socket.on('opponentPosition', function (data) {
                if(data.clientId != clientId){
                    if(data.playerAnimation) enemy.animations.play('walk', 15, true);
                    else {
                        enemy.body.x = data.playerPosX;
                        enemy.animations.stop('walk');
                    }
                    enemy.body.velocity.x = data.playerVelocity;
                }
            });
        }

        if(rightkey.isDown || leftkey.isDown){
            enemyStartBool = true;
            player.animations.play('walk', 20, true);
            if(!(rightkey.isDown && leftkey.isDown)){
                if(rightkey.isDown && rightTapBool){
                    if(player.body.velocity.x > 1000) player.body.velocity.x = 1000;
                    else player.body.velocity.x += 50;
                    if(whichGameMode == 2){
                        socket.emit('playerPosition',{
                            clientId : clientId,
                            roomNo : roomNo,
                            playerVelocity : player.body.velocity.x,
                            playerAnimation : true,
                            playerPosX : player.body.x
                        });
                    }
                    rightTapBool = false;
                    leftTapBool = true;
                } else if(leftkey.isDown && leftTapBool) {
                    if(player.body.velocity.x > 1000) player.body.velocity.x = 1000;
                    else player.body.velocity.x += 50;
                    if(whichGameMode == 2){
                        socket.emit('playerPosition',{
                            clientId : clientId,
                            roomNo : roomNo,
                            playerVelocity : player.body.velocity.x,
                            playerAnimation : true,
                            playerPosX : player.body.x
                        });
                    }
                    rightTapBool = true;
                    leftTapBool = false;
                }
            }
        }
        else{
            player.animations.stop('walk');
            player.frameName = 'sprite7';
            if(player.body.velocity.x > 0) player.body.velocity.x -= 30;
            else player.body.velocity.x = 0;
            if(whichGameMode == 2){
                socket.emit('playerPosition',{
                    clientId : clientId,
                    roomNo : roomNo,
                    playerVelocity : player.body.velocity.x,
                    playerAnimation : false,
                    playerPosX : player.body.x
                });
            }
        }

    },
    playerWon: function (player, star) {
        console.log('player Won');
        if(whichGameMode == 2){
            socket.emit('closeConnection', {clientId : clientId, roomNo : roomNo});
            socket.disconnect();
        }
        star.kill();
        game.state.start('playerWin');
    },
    enemyWon : function (enemy, star) {
        console.log('enemy Won');
        if(whichGameMode == 2){
            socket.emit('closeConnection', {clientId : clientId, roomNo : roomNo});
            socket.disconnect();
        }
        star.kill();
        game.state.start('enemyWin');
    }
};