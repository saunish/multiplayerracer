console.log('game');
var game = new Phaser.Game(960, 640, Phaser.AUTO, 'game');

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('multiplayer', multiplayerState);
game.state.add('createServer', createServerState);
game.state.add('wait', waitState);
game.state.add('join', joinState);
game.state.add('gamePlay', gamePlayState);
game.state.add('playerWin', playerWinState);
game.state.add('enemyWin', enemyWinState);
game.state.add('leaderboard', leaderboardState);

game.state.start('boot');