var joinState = {
    create: function () {

        startTheGameBool = false;

        socket.on('socketDetails', function (data) {
            roomNo = data.roomNo;
            isFirst = data.isFirst;
            clientId = data.clientId;
        });

        socket.on('connectionSuccess', function (data) {
            socket.emit('playerDetail', {
                roomNo : roomNo,
                clientId : clientId,
                playerName : playerName,
                playerCharacter : playerCharacter
            });
        });

        socket.on('opponentDetail', function (data) {
            if(data.clientId != clientId){
                opponentName = data.playerName;
                opponentCharacter = data.playerCharacter;
                game.state.start('wait');
            }
        });

        game.add.text((game.width*10)/100, (game.height*10)/100, 'waiting for other player to join', {
            font: '30px Arial', fill: '#ffffff'
        });

    }
};