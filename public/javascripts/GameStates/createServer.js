var createServerState = {
    create: function () {
        console.log('CreateServer');

        socket = io();

        //no of players
        game.add.text((game.width*10)/100, (game.height*10)/100, 'Select No of Opponent', {
            font: '30px Arial', fill: '#ffffff'
        });

        game.add.text((game.width*10)/100, (game.height*15)/100, '1:', {
            font: '20px Arial', fill: '#ffffff'
        });
        onePlayer = game.add.button( (game.width*13)/100, (game.height*15)/100, 'radioButton', this.onePlayerSelect);
        onePlayer.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*20)/100, '2:', {
            font: '20px Arial', fill: '#ffffff'
        });
        twoPlayer = game.add.button( (game.width*13)/100, (game.height*20)/100, 'radioButton', this.twoPlayerSelect);
        twoPlayer.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*25)/100, '3:', {
            font: '20px Arial', fill: '#ffffff'
        });
        threePlayer = game.add.button( (game.width*13)/100, (game.height*25)/100, 'radioButton', this.threePlayerSelect);
        threePlayer.scale.setTo(0.5, 0.5);

        noOfPlayerSelector = game.add.sprite( (game.width*13.5)/100, (game.height*15.5)/100, 'radioButtonSelector');
        noOfPlayerSelector.scale.setTo(0.5, 0.5);
        game.physics.enable(noOfPlayerSelector, Phaser.Physics.ARCADE);

        noOfPlayer = 1;
        noOfPlayerBool = false;

        // select Background
        game.add.text((game.width*10)/100, (game.height*30)/100, 'Select Background', {
            font: '30px Arial', fill: '#ffffff'
        });
        game.add.text((game.width*10)/100, (game.height*35)/100, 'Desert', {
            font: '20px Arial', fill: '#ffffff'
        });
        desertButton = game.add.button( (game.width*20)/100, (game.height*35)/100, 'radioButton', this.desertSelect);
        desertButton.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*40)/100, 'Grass', {
            font: '20px Arial', fill: '#ffffff'
        });
        grassButton = game.add.button( (game.width*20)/100, (game.height*40)/100, 'radioButton', this.grassSelect);
        grassButton.scale.setTo(0.5, 0.5);

        backgroundSelector = game.add.sprite( (game.width*20.5)/100, (game.height*35.5)/100, 'radioButtonSelector');
        backgroundSelector.scale.setTo(0.5, 0.5);
        game.physics.enable(backgroundSelector, Phaser.Physics.ARCADE);

        whichBackground = 1;
        backgroundSelectorBool = false;

        // player select
        game.add.text((game.width*60)/100, (game.height*5)/100, 'Select Your Player', {
            font: '30px Arial', fill: '#ffffff'
        });

        playerCharacter = 'dude';
        playerSelectBool = false;

        playerSelect = game.add.text((game.width*10)/100, (game.height*50)/100, 'Your Player is', {
            font: '30px Arial', fill: '#ffffff'
        });

        displayPlayerCharacterName = game.add.text((game.width*10)/100, (game.height*55)/100, playerCharacter, {
            font: '25px Arial', fill: '#ffffff'
        });

        dude = game.add.button( (game.width*60)/100, (game.height*20)/100, 'dude', this.dudeSelect);
        dude.frameName = 'sprite7';
        dude.animations.add('action', ['sprite6', 'sprite8']);

        soldier = game.add.button( (game.width*80)/100, (game.height*20)/100, 'soldier', this.soldierSelect);
        soldier.frameName = 'sprite6';
        soldier.animations.add('action', ['sprite7', 'sprite8']);

        adventurer = game.add.button( (game.width*60)/100, (game.height*40)/100, 'adventurer', this.adventurerSelect);
        adventurer.frameName = 'sprite7';
        adventurer.animations.add('action', ['sprite6', 'sprite8']);

        zombie = game.add.button( (game.width*80)/100, (game.height*40)/100, 'zombie', this.zombieSelect);
        zombie.frameName = 'sprite6';
        zombie.animations.add('action', ['sprite7', 'sprite8']);

        dude.animations.play('action', 10, true);
        zombie.animations.play('action', 10, true);
        soldier.animations.play('action', 10, true);
        adventurer.animations.play('action', 10, true);


        game.add.button( (game.width*20)/100, (game.height*90)/100, 'yellowButton', this.createServerSelect);
        game.add.text((game.width*22)/100, (game.height*92)/100, 'Create Server', {
            font: '15px Arial', fill: '#000000'
        });

        socket.on('connect', function () {
            console.log(socket.id);
        });

        game.add.button( (game.width*85)/100, (game.height*85)/100, 'homeButton', this.backToHome);
    },
    dudeSelect: function () {
        if(playerCharacter != 'dude'){
            playerCharacter = 'dude';
            playerSelectBool = true;
        }
    },
    zombieSelect: function () {
        if(playerCharacter != 'zombie'){
            playerCharacter = 'zombie';
            playerSelectBool = true;
        }
    },
    soldierSelect: function () {
        if(playerCharacter != 'soldier'){
            playerCharacter = 'soldier';
            playerSelectBool = true;
        }
    },
    adventurerSelect: function () {
        if(playerCharacter != 'adventurer'){
            playerCharacter = 'adventurer';
            playerSelectBool = true;
        }
    },
    backToHome: function () {
        socket.disconnect();
        game.state.start('menu');
    },
    onePlayerSelect: function () {
        if(noOfPlayer != 1){
            noOfPlayer = 1;
            noOfPlayerBool = true;
        }
    },
    twoPlayerSelect: function () {
        if(noOfPlayer != 2){
            noOfPlayer = 2;
            noOfPlayerBool = true;
        }
    },
    threePlayerSelect: function () {
        if(noOfPlayer != 3){
            noOfPlayer = 3;
            noOfPlayerBool = true;
        }
    },
    desertSelect: function () {
        if(whichBackground != 1){
            whichBackground = 1;
            backgroundSelectorBool = true;
        }
    },
    grassSelect: function () {
        if(whichBackground != 2){
            whichBackground = 2;
            backgroundSelectorBool = true;
        }
    },
    createServerSelect: function () {
        socket.emit('joinRoom', {clientId : socket.id});
        game.state.start('wait');
    },
    update: function () {
        if(playerSelectBool){
            displayPlayerCharacterName.text = playerCharacter;
            playerSelectBool = false;
        }
        if(noOfPlayerBool){
            if(noOfPlayer == 1) noOfPlayerSelector.body.y = (game.height*15.5)/100;
            if(noOfPlayer == 2) noOfPlayerSelector.body.y = (game.height*20.5)/100;
            if(noOfPlayer == 3) noOfPlayerSelector.body.y = (game.height*25.5)/100;
        }
        if(backgroundSelectorBool){
            if(whichBackground == 1) backgroundSelector.body.y = (game.height*35.5)/100;
            if(whichBackground == 2) backgroundSelector.body.y = (game.height*40.5)/100;
        }
    }
};