var menuState = {
    create: function () {
        console.log('menu');
        game.stage.backgroundColor = '#000000';

        game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        fullScreen = game.add.button((game.width*95)/100, (game.height*0)/100, 'fullScreen', this.gofull);
        fullScreen.scale.setTo(0.5, 0.5);
        fullScreen.visible = true;


        var welcome = game.add.text((game.width*10)/100, (game.height*20)/100, 'Welcome to\nRacer Game', {
            font: '40px Arial', fill: '#ffffff'
        });


        startGameButton = game.add.button( (game.width*80)/100, (game.height*55)/100, 'gameStartIcon', this.startTheGame);



        //game Difficulty level select
        var difficultyText = game.add.text((game.width*10)/100, (game.height*75)/100, 'Select difficult level', {
            font: '30px Arial', fill: '#ffffff'
        });

        game.add.text((game.width*10)/100, (game.height*80)/100, 'High', {
            font: '20px Arial', fill: '#ffffff'
        });
        highLevel = game.add.button( (game.width*15)/100, (game.height*80)/100, 'radioButton', this.highLevelSelect);
        highLevel.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*85)/100, 'Mid', {
            font: '20px Arial', fill: '#ffffff'
        });
        midLevel = game.add.button( (game.width*15)/100, (game.height*85)/100, 'radioButton', this.midLevelSelect);
        midLevel.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*90)/100, 'Low', {
            font: '20px Arial', fill: '#ffffff'
        });
        lowLevel = game.add.button( (game.width*15)/100, (game.height*90)/100, 'radioButton', this.lowLevelSelect);
        lowLevel.scale.setTo(0.5, 0.5);

        levelSelector = game.add.sprite( (game.width*15.5)/100, (game.height*85.5)/100, 'radioButtonSelector');
        levelSelector.scale.setTo(0.5, 0.5);
        game.physics.enable(levelSelector, Phaser.Physics.ARCADE);

        levelBool = false;
        whichLevel = 2;

        //background select

        game.add.text((game.width*10)/100, (game.height*60)/100, 'Select Background', {
            font: '30px Arial', fill: '#ffffff'
        });
        game.add.text((game.width*10)/100, (game.height*65)/100, 'Desert', {
            font: '20px Arial', fill: '#ffffff'
        });
        desertButton = game.add.button( (game.width*20)/100, (game.height*65)/100, 'radioButton', this.desertSelect);
        desertButton.scale.setTo(0.5, 0.5);

        game.add.text((game.width*10)/100, (game.height*70)/100, 'Grass', {
            font: '20px Arial', fill: '#ffffff'
        });
        grassButton = game.add.button( (game.width*20)/100, (game.height*70)/100, 'radioButton', this.grassSelect);
        grassButton.scale.setTo(0.5, 0.5);

        backgroundSelector = game.add.sprite( (game.width*20.5)/100, (game.height*65.5)/100, 'radioButtonSelector');
        backgroundSelector.scale.setTo(0.5, 0.5);
        game.physics.enable(backgroundSelector, Phaser.Physics.ARCADE);

        whichBackground = 1;
        backgroundSelectorBool = false;


        //players

        game.add.text((game.width*60)/100, (game.height*5)/100, 'Select Your Player', {
            font: '30px Arial', fill: '#ffffff'
        });

        playerCharacter = 'dude';
        playerSelectBool = false;

        playerSelect = game.add.text((game.width*10)/100, (game.height*40)/100, 'Your Player is', {
            font: '30px Arial', fill: '#ffffff'
        });

        displayPlayerCharacterName = game.add.text((game.width*10)/100, (game.height*45)/100, playerCharacter, {
            font: '25px Arial', fill: '#ffffff'
        });

        dude = game.add.button( (game.width*60)/100, (game.height*20)/100, 'dude', this.dudeSelect);
        dude.frameName = 'sprite7';
        dude.animations.add('action', ['sprite6', 'sprite8']);

        soldier = game.add.button( (game.width*80)/100, (game.height*20)/100, 'soldier', this.soldierSelect);
        soldier.frameName = 'sprite6';
        soldier.animations.add('action', ['sprite7', 'sprite8']);

        adventurer = game.add.button( (game.width*60)/100, (game.height*40)/100, 'adventurer', this.adventurerSelect);
        adventurer.frameName = 'sprite7';
        adventurer.animations.add('action', ['sprite6', 'sprite8']);

        zombie = game.add.button( (game.width*80)/100, (game.height*40)/100, 'zombie', this.zombieSelect);
        zombie.frameName = 'sprite7';
        zombie.animations.add('action', ['sprite7', 'sprite8']);
        zombie.visible = false;

        dude.animations.play('action', 10, true);
        soldier.animations.play('action', 10, true);
        adventurer.animations.play('action', 10, true);
        zombie.animations.play('action', 10, true);

        //game mode select

        game.add.text((game.width*60)/100, (game.height*70)/100, 'Select Your Game Mode', {
            font: '30px Arial', fill: '#ffffff'
        });

        game.add.text((game.width*60)/100, (game.height*75)/100, 'Single Player', {
            font: '20px Arial', fill: '#ffffff'
        });
        singlePlayerButton = game.add.button( (game.width*75)/100, (game.height*75)/100, 'radioButton', this.singleplayerSelect);
        singlePlayerButton.scale.setTo(0.5, 0.5);

        game.add.text((game.width*60)/100, (game.height*80)/100, 'Multi Player', {
            font: '20px Arial', fill: '#ffffff'
        });
        multiPlayerButton = game.add.button( (game.width*75)/100, (game.height*80)/100, 'radioButton', this.multiplayerSelect);
        multiPlayerButton.scale.setTo(0.5, 0.5);


        gameModeSelector = game.add.sprite( (game.width*75.5)/100, (game.height*75.5)/100, 'radioButtonSelector');
        gameModeSelector.scale.setTo(0.5, 0.5);
        game.physics.enable(gameModeSelector, Phaser.Physics.ARCADE);

        whichGameMode = 1;
        gameModeBool = false;


        //enter Name
        game.add.text((game.width*60)/100, (game.height*60)/100, 'Type Your Name', {
            font: '20px Arial', fill: '#ffffff'
        });
        game.add.text((game.width*60)/100, (game.height*66)/100, '________________', {
            font: '20px Arial', fill: '#ffffff'
        });

        playerNameArray = [];
        playerName = '';

        displayPlayerName = game.add.text((game.width*60)/100, (game.height*65)/100, playerName, {
            font: '25px Arial', fill: '#ffffff'
        });

        game.add.text((game.width*60)/100, (game.height*90)/100, 'Note : Enter min\n 3 character for Name', {
            font: '20px Arial', fill: '#ffffff'
        });


        game.input.keyboard.addCallbacks(this, null, null, this.keyPress);

        $(document).bind("keydown", function (e) {
            if (e.keyCode == 8) { // backspace
                e.preventDefault();
                playerNameArray.pop();
                playerName = playerNameArray.join("");
            }
        });
    },
    keyPress: function (char) {
        playerNameArray.push(char);
        playerName = playerNameArray.join("");
    },
    dudeSelect: function () {
        if(playerCharacter != 'dude'){
            playerCharacter = 'dude';
            playerSelectBool = true;
        }
    },
    soldierSelect: function () {
        if(playerCharacter != 'soldier'){
            playerCharacter = 'soldier';
            playerSelectBool = true;
        }
    },
    adventurerSelect: function () {
        if(playerCharacter != 'adventurer'){
            playerCharacter = 'adventurer';
            playerSelectBool = true;
        }
    },
    zombieSelect: function () {
        if(playerCharacter != 'zombie'){
            playerCharacter = 'zombie';
            playerSelectBool = true;
        }
    },
    highLevelSelect: function () {
        if(whichLevel != 3){
            levelBool = true;
            whichLevel = 3;
        }
    },
    midLevelSelect: function () {
        if(whichLevel != 2){
            levelBool = true;
            whichLevel = 2;
        }
    },
    lowLevelSelect: function () {
        if(whichLevel != 1){
            levelBool = true;
            whichLevel = 1;
        }
    },
    desertSelect: function () {
        if(whichBackground != 1){
            whichBackground = 1;
            backgroundSelectorBool = true;
        }
    },
    grassSelect: function () {
        if(whichBackground != 2){
            whichBackground = 2;
            backgroundSelectorBool = true;
        }
    },
    startTheGame: function () {
        if(!game.device.desktop){
            if(whichGameMode == 1) game.state.start('gamePlay');
            else if(whichGameMode == 2){
                socket = io();
                socket.on('connect', function () {
                    socket.emit('joinRoom', {clientId : socket.id});
                });
                game.state.start('join');
            }
        }
        else{
            if(playerName.length > 2){
                if(whichGameMode == 1) game.state.start('gamePlay');
                else if(whichGameMode == 2){
                    socket = io();
                    socket.on('connect', function () {
                        socket.emit('joinRoom', {clientId : socket.id});
                    });
                    game.state.start('join');
                }
            }
        }
    },
    singleplayerSelect: function () {
        if(whichGameMode != 1){
            playerCharacter = 'dude';
            zombie.visible = false;
            playerSelectBool = true;
            whichGameMode = 1;
            gameModeBool = true;
        }
    },
    multiplayerSelect: function () {
        if(whichGameMode != 2){
            playerCharacter = 'dude';
            zombie.visible = true;
            playerSelectBool = true;
            whichGameMode = 2;
            gameModeBool = true;
        }
    },
    update: function () {
        if(levelBool){
            if(whichLevel == 1) levelSelector.body.y = (game.height*90.5)/100;
            if(whichLevel == 2) levelSelector.body.y = (game.height*85.5)/100;
            if(whichLevel == 3) levelSelector.body.y = (game.height*80.5)/100;
            levelBool = false;
        }
        if(playerSelectBool){
            displayPlayerCharacterName.text = playerCharacter;
            playerSelectBool = false;
        }
        if(backgroundSelectorBool){
            if(whichBackground == 1) backgroundSelector.body.y = (game.height*65.5)/100;
            if(whichBackground == 2) backgroundSelector.body.y = (game.height*70.5)/100;
            backgroundSelectorBool = false;
        }
        if(gameModeBool){
            if(whichGameMode == 1) gameModeSelector.body.y = (game.height*75.5)/100;
            if(whichGameMode == 2) gameModeSelector.body.y = (game.height*80.5)/100;
            gameModeBool = false;
        }

        displayPlayerName.text = playerName;
    },
    gofull: function() {
        if (game.scale.isFullScreen) game.scale.stopFullScreen();
        else game.scale.startFullScreen(false);
    }
};