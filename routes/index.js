module.exports = io => {
  const express = require("express");
  const router = express.Router();

  /* GET home page. */
  router.get("/", (req, res, next) => {
    res.render("index", { title: "Express" });
  });

  io.on("connection", socket => {
    console.log(socket.id + " is connected");

    var roomno = 1;
    socket.on("joinRoom", data => {
      if (
        io.nsps["/"].adapter.rooms["room-" + roomno] &&
        io.nsps["/"].adapter.rooms["room-" + roomno].length > 1
      )
        roomno++;

      socket.join("room-" + roomno);

      if (io.nsps["/"].adapter.rooms["room-" + roomno].length == 1)
        socket.emit("socketDetails", {
          roomNo: roomno,
          clientId: data.clientId,
          isFirst: true
        });
      else {
        socket.emit("socketDetails", {
          roomNo: roomno,
          clientId: data.clientId,
          isFirst: false
        });
        io.sockets
          .in("room-" + roomno)
          .emit("connectionSuccess", { startTheGame: true });
      }
    });

    socket.on("playerDetail", data => {
      io.sockets.in("room-" + data.roomNo).emit("opponentDetail", {
        clientId: data.clientId,
        playerName: data.playerName,
        playerCharacter: data.playerCharacter
      });
    });

    socket.on("playerPosition", data => {
      io.sockets.in("room-" + data.roomNo).emit("opponentPosition", {
        clientId: data.clientId,
        playerVelocity: data.playerVelocity,
        playerAnimation: data.playerAnimation,
        playerPosX: data.playerPosX
      });
    });

    socket.on("closeConnection", data => {
      socket.leave("room-" + data.roomNo);
    });

    socket.on("disconnect", () => {
      console.log(socket.id + "has left");
    });
  });
  return router;
};
